#!/bin/sh

# Don't allow a port that's in the privileged port range
if [ "$PORT" -lt 1024 ]; then
    echo "[-] You must set the port to a a port above 1023"
    echo "  [*] ex.) docker run -it --rm -e CONCURRENT_DOWNLOADS=32 -e PORT=8080 -p 8080:8080 alpine-mirror:latest"
    exit 1
fi

# Calculate size requirement, if not running in DEBUG mode
if [ "$DEBUG" -ne 0 ]; then
    echo "[*] DEBUG MODE IS ENABLED, SKIPPING SIZE CHECK (PULLING LIMITED NUMBER OF PACKAGES)"
else
    total_size_in_bytes=0
    for alpine_version in $(printenv | grep "VERSIONS=" | cut -d"=" -f2 | sed "s| |\n|g")
    do
        for branch in "main" "community"
        do
            for arch in $(printenv | grep "ARCHS=" | cut -d"=" -f2 | sed "s| |\n|g")
            do
                mkdir -p "/mirror/alpine/$alpine_version/$branch/$arch/"
                cd "/mirror/alpine/$alpine_version/$branch/$arch/" || exit 1
                curl -sS "https://$CDN/alpine/$alpine_version/$branch/$arch/APKINDEX.tar.gz" -o APKINDEX.tar.gz
                tar -xf APKINDEX.tar.gz
                size_in_bytes=$(grep "^S:" APKINDEX | sed "s|S:||g" | paste -sd+ - | bc)
                total_size_in_bytes=$((total_size_in_bytes + size_in_bytes))
                rm -rf APKINDEX APKINDEX.tar.gz DESCRIPTION
            done
        done
    done
    overage=$(echo "scale=2; $PERCENT_OVERAGE/100 + 1" | bc)
    total_size_in_gb=$(echo "scale=2; $total_size_in_bytes/1073741824 * $overage" | bc)
    size_available=$(df -h | grep "/$" | rev | cut -d"G" -f2 | rev | sed "s| ||g")
    echo "[*] Size required (including overage): $total_size_in_gb GB"
    echo "[*] Size available: $size_available GB"

    # bc returns 1 for true, 0 for false, but the opposite is true for shell
    if [ "$(echo "scale=2; $total_size_in_gb > $size_available" | bc)" ]; then
        echo "[+] Sufficient space! Building mirror..."
    else
        echo "[*] Insufficient space! Exiting..."
        exit 1
    fi
fi


# Sync the mirror to requested CDN
sh /app/update_mirror.sh

# Update the supervisor configuration to start the http server on the port requested by the user
sed -i "s|<DARKHTTPD_PORT>|$PORT|g" /app/supervisord.conf 

# Execute the cmd in the dockerfile - use supervisor to keep daemons alive
exec "$@"
