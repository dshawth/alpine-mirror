#!/bin/sh

alias curl="curl -sS --connect-timeout 10 --max-time 120 --retry 3 --retry-delay 0 --retry-max-time 120"

# ARGS: alpine_version, branch, arch
id_packages()
{
    # Create folders if they don't exist
    incoming_folder="/incoming/alpine/$1/$2/$3"
    mkdir -p "$incoming_folder/"
    mkdir -p "/mirror/alpine/$1/$2/$3/"

    # Grab tarball for this alpine_version, branch, arch
    if [ "$DEBUG" -ne 0 ]; then
        echo "[*] INCOMING FOLDER: $incoming_folder"
    fi

    curl https://"$CDN"/alpine/"$1"/"$2"/"$3"/APKINDEX.tar.gz \
        -o "$incoming_folder"/APKINDEX_INCOMING.tar.gz

    # Hash the tarballs. If they're the same, we don't need to do anything at all.
    if [ -f /mirror/alpine/"$1"/"$2"/"$3"/APKINDEX.tar.gz ]; then
        incoming_hash=$(sha1sum "$incoming_folder"/APKINDEX_INCOMING.tar.gz | cut -d" " -f1)
        current_mirror_hash=$(sha1sum /mirror/alpine/"$1"/"$2"/"$3"/APKINDEX.tar.gz | cut -d" " -f1)
        if [ "$incoming_hash" = "$current_mirror_hash" ]; then
            echo "[+] Upstream CDN hasn't changed since last update! UPDATE COMPLETE."
            rm -rf "$incoming_folder/:?"
            exit 0
        fi
    else
        # This is the first time this index has been requested
        cd "$incoming_folder/" || exit 1
        tar -xf APKINDEX_INCOMING.tar.gz
        grep "^P:" APKINDEX | cut -d":" -f2 > packages.txt
        grep "^V:" APKINDEX | cut -d":" -f2 > versions.txt
        paste packages.txt versions.txt -d"-" > apk_packages.txt
        if [ "$DEBUG" -ne 0 ]; then
            echo "[*] DEBUG MODE IS ENABLED, ONLY SLATING FIRST 20 PACKAGES FOR DOWNLOAD"
            head -n 20 apk_packages.txt > tmp.txt && mv tmp.txt apk_packages.txt
        fi
        
        # Save the file paths we need to pull the packages from the upstream CDN
        while read -r line || [ -n "$line" ]; do
            echo "/alpine/$1/$2/$3/$line.apk" >> /app/packages_to_pull.txt
        done < apk_packages.txt

        # Move the incoming tarball to the mirror folder
        mv "$incoming_folder"/APKINDEX_INCOMING.tar.gz  /mirror/alpine/"$1"/"$2"/"$3"/APKINDEX.tar.gz
        return
    fi

    # Extract data from tarballs
    cd "$incoming_folder/" || exit 1
    tar -xf APKINDEX_INCOMING.tar.gz
    mv APKINDEX APKINDEX.incoming
    grep -e "^P:" -e "^C:" -e "^V:" APKINDEX.incoming > incoming_packages.txt
    tar -xf /mirror/alpine/"$1"/"$2"/"$3"/APKINDEX.tar.gz -C .
    grep -e "^P:" -e "^C:" -e "^V:" APKINDEX > current_packages.txt

    # Find which packages are different
    diff current_packages.txt incoming_packages.txt | grep -e "+C:" -A 2 | grep -e "P:" -e "V:" > diff.txt
    grep "P:" diff.txt | cut -d":" -f2 > packages.txt
    grep "V:" diff.txt | cut -d":" -f2 > versions.txt
    paste packages.txt versions.txt -d"-" > apk_packages.txt # packagename-version

    # Save the file paths we need to pull the packages from the upstream CDN
    while read -r line || [ -n "$line" ]; do
        echo "/alpine/$1/$2/$3/$line.apk" >> /app/packages_to_pull.txt # /alpine/v3.16/main/x86_64/packagename-version.apk
    done < apk_packages.txt

    # Move the incoming tarball to the mirror folder
    mv "$incoming_folder"/APKINDEX_INCOMING.tar.gz  /mirror/alpine/"$1"/"$2"/"$3"/APKINDEX.tar.gz
}    


# ============================================================
# EXECUTION BEGINS
# ============================================================

# ID all of the packages we need to update
echo "[*] Identifying packages to pull"
for alpine_version in $(printenv | grep "VERSIONS=" | cut -d"=" -f2 | sed "s| |\n|g")
do
    for branch in "main" "community"
    do
        for arch in $(printenv | grep "ARCHS=" | cut -d"=" -f2 | sed "s| |\n|g")
        do
            id_packages "$alpine_version" "$branch" "$arch"
        done
    done
done

# Go pull them all
echo "[*] Downloading packages... this might take a minute. Grab a coffee."
while read -r PACKAGE_PATH || [ -n "$PACKAGE_PATH" ]; do
    while [ "$(ps axo user:20,command | grep "$(whoami)" | grep -c "curl")" -ge "$CONCURRENT_DOWNLOADS" ]; do
        printf '.'
        sleep 1
    done
    (
        ALPINE_VERSION=$(echo "$PACKAGE_PATH" | rev | cut -d"/" -f4 | rev)
        BRANCH=$(echo "$PACKAGE_PATH" | rev | cut -d"/" -f3 | rev)
        ARCH=$(echo "$PACKAGE_PATH" | rev | cut -d"/" -f2 | rev)
        PACKAGE_NAME=$(echo "$PACKAGE_PATH" | rev | cut -d"/" -f1 | rev)
        WRITE_LOCATION="/mirror/alpine/$ALPINE_VERSION/$BRANCH/$ARCH/$PACKAGE_NAME"
        if [ "$DEBUG" -ne 0 ]; then
            echo "[*] Downloading: $PACKAGE_PATH"
        fi
        
        if ! curl "https://$CDN$PACKAGE_PATH" -o "$WRITE_LOCATION"; then
            echo "[-] CURL FAILED FOR: https://$CDN$PACKAGE_PATH" | tee -a "/app/failed.txt"
        fi
        verified=$(apk verify "$WRITE_LOCATION" | grep -q "0 - OK")
        if ! $verified; then
            echo "[-] APK VERIFY FAILED FOR:$WRITE_LOCATION" >> "/app/failed.txt"
        fi
    ) &
done < "/app/packages_to_pull.txt"

wait 

rm "/app/packages_to_pull.txt"

# Print output if there were failures and exit
if [ -f "/app/failed.txt" ]; then
    echo "[*] List of packages that failed: "
    cat /app/failed.txt
    exit 1
else
    echo "[+] All packages succesfully mirrored!"
    exit 0
fi
