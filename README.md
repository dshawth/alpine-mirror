# Alpine Mirror

An Alpine-based, all-in-one Alpine package mirror container.

## Run this mirror:

```sh
# build command
docker build -t alpine-mirror .

# simple execution
docker run --rm -p 8080:8080 alpine-mirror:latest

# Customized execution with optional arguments
docker run --rm \
    --env VERSIONS='v3.16' \
    --env ARCHS='x86_64' \
    --env CDN='dl-cdn.alpinelinux.org' \
    --env PERCENT_OVERAGE=10 \
    --env CRON='0 * * * *' \
    --env CONCURRENT_DOWNLOADS=16 \
    -p 8080:8080 \
    alpine-mirror:latest

```

## Use this mirror:

Update `/etc/apk/repositories` in your Alpine container to point to this mirror.

```sh
echo "{THIS_MIRROR}" | cat - /etc/apk/repositories > /tmp/out && mv /tmp/out /etc/apk/repositories

# https://dl-cdn.alpinelinux.org/alpine/v3.16/main
# https://dl-cdn.alpinelinux.org/alpine/v3.16/community

```

## References:

- https://mirrors.alpinelinux.org/
- https://wiki.alpinelinux.org/wiki/How_to_setup_a_Alpine_Linux_mirror
- https://hub.docker.com/_/alpine
- https://wiki.alpinelinux.org/wiki/Apk_spec
