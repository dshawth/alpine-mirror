FROM alpine:3.16

LABEL maintainer="CSD-A"

# Space separated list of versions to mirror
ENV VERSIONS='v3.16' 

# Space separated list of architectures to mirror
ENV ARCHS='x86_64'

# Upstream CDN to pull packages from
ENV CDN='dl-cdn.alpinelinux.org'

# Percent of the size of the content downloaded that should be left over.
# ** This enables your mirror to handle a case where the size of the packages you're mirroring has increased between updates.
ENV PERCENT_OVERAGE=25

# The schedule the mirror will use to update itself
ENV CRON='0 * * * *'

# Set the number of maximum concurrent package downloads
# WARNING: SETTING THIS VALUE TOO HIGH MAY RESULT IN SOME LARGE PACKAGES FAILING TO DOWNLOAD (INSUFFICIENT TIME/BANDWIDTH)
ENV CONCURRENT_DOWNLOADS=6

# Set to 1 to see debug info
# WARNING: SIGNIFICANT PERFORMANCE IMPACT
ENV DEBUG=0

# The port to serve the mirror content on.
# TODO: FAIL IF PRIVILEGED PORT NUMBER CHOSEN
ENV PORT=8080


RUN apk upgrade --no-cache --update && \
    apk add curl darkhttpd supervisor

RUN [ "mkdir", "-p", "/app", "/mirror", "/incoming" ]

COPY [ "./app/*", "/app/" ]

RUN [ "chown", "darkhttpd", "/app", "/mirror", "/incoming", "/etc/apk" ]

RUN [ "chmod", "+x", "/app/entry.sh" ]

RUN [ "chmod", "+x", "/app/update_mirror.sh" ]

EXPOSE $PORT

USER darkhttpd

# Write update schedule into cron
RUN mkdir /tmp/crontabs \
    && echo 'SHELL=/bin/sh' > /tmp/crontabs/darkhttpd \
    && echo "$CRON /bin/sh /app/update_mirror.sh > /proc/1/fd/1 2> /proc/1/fd/2" >> /tmp/crontabs/darkhttpd

ENTRYPOINT [ "sh", "/app/entry.sh" ]

CMD [ "/usr/bin/supervisord", "-l", "/app/supervisord.log", "-c", "/app/supervisord.conf" ]
